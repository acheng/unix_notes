#!/bin/env bash

## this script was originally written to check the number of 
## connections on each perforce instances running on a host
## whose config was stored in /opt/perforce/config/*.cfg
## 
LOGDIR="/opt/perforce/.conlogs"
LOGFILE="$LOGDIR/con.$(date '+%Y-%m-%d').$(hostname -s).log"
CHK_TIME=$(date '+%m/%d/%Y %H:%M:%S')

#stop execution after a date
# in case I setup cronjob using this 
# but forget to stop it. 

EPOC_SEC=$(date '+%s')
STOP_EPOC=1569918723
# when to stop checking, 
# Tuesday, October 1, 2019 4:32:03 PM GMT+08:00, random choice
if [[ $EPOC_SEC -gt $STOP_EPOC ]]; then
        exit 7
fi


##
if [[ ! -d $LOGDIR ]]; then
        exit
fi

## get port number that is part of the config file name
for i in $(ls -l /opt/perforce/config | egrep -o [0-9]{4}.cfg | egrep -o [0-9]{4});
do
		[ "X$i" == "X" ] && continue
        number_conns=$(netstat -atn | awk '$4 ~/:'$i'/'|wc -l)
        echo "$i, $number_conns, $CHK_TIME" >> "$LOGFILE"
done
