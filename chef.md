

| item  |  folder name |   description |
| ---------- |:--------------|:------------------------|
|Recipes	| recipes/| A recipe is the most fundamental configuration element within the organization. A recipe<br/> <li> Is authored using Ruby<br/> <li> Is mostly a collection of resources, defined using patterns (resource names, attribute-value pairs, and actions); helper code is added around this using Ruby, when needed <br/><li>Must define everything that is required to configure part of a system<br/><li>Must be stored in a cookbook<br/><li>May be included in another recipe<br><li>May use the results of a search query and read the contents of a data bag (including an encrypted data bag)<br/><li>May have a dependency on one (or more) recipes<br/><li>Must be added to a run-list before it can be used by Chef Infra Client<br/><li>Is always executed in the same order as listed in a run-list
|Attributes	|attributes/|	An attribute can be defined in a cookbook (or a recipe) and then used to override the default settings on a node. When a cookbook is loaded during a Chef Infra Client run, these attributes are compared to the attributes that are already present on the node. Attributes that are defined in attribute files are first loaded according to cookbook order. For each cookbook, attributes in the default.rb file are loaded first, and then additional attribute files (if present) are loaded in lexical sort order. When the cookbook attributes take precedence over the default attributes, Chef Infra Client applies those new settings and values during a Chef Infra Client run on the node.|
|Files	|files/	|A file distribution is a specific type of resource that tells a cookbook how to distribute files, including by node, by platform, or by file version.|
|Libraries|	libraries/|	A library allows the use of arbitrary Ruby code in a cookbook, either as a way to extend the Chef Infra Client language or to implement a new class.|
|Custom Resources|	resources/|	A custom resource is an abstract approach for defining a set of actions and (for each action) a set of properties and validation parameters.|
|Templates|	templates/|	A template is a file written in markup language that uses Ruby statements to solve complex configuration scenarios.|
|Ohai Plugins|	ohai/|	Custom Ohai plugins can be written to load additional information about your nodes to be used in recipes. This requires Chef Infra Server 12.18.14 or later.|
|Metadata|	metadata.rb	|This file contains information about the cookbook such as the cookbook name, description, and version.|



About Resources
=====================================================

A resource is a statement of configuration policy that:

* Describes the desired state for a configuration item
* Declares the steps needed to bring that item to the desired state
* Specifies a resource type---such as ``package``, ``template``, or ``service``
* Lists additional details (also known as resource properties), as necessary
* Are grouped into recipes, which describe working configurations


Resource Syntax
=====================================================
A resource is a Ruby block with four components: a type, a name, one (or more) properties (with values), and one (or more) actions. The syntax for a resource is like this:

```ruby

   type 'name' do
      attribute 'value'
      action :type_of_action
   end
```
Every resource has its own set of actions and properties. Most properties have default values. Some properties are available to all resources, for example those used to send notifications to other resources and guards that help ensure that some resources are idempotent.

For example, a resource that is used to install a tar.gz package for version 1.16.1 may look something like this:

```ruby
   package 'tar' do
     version '1.16.1'
     action :install
   end
```
All actions have a default value. Only non-default behaviors of actions and properties need to be specified. For example, the **package** resource's default action is ``:install`` and the name of the package defaults to the ``name`` of the resource. Therefore, it is possible to write a resource block that installs the latest tar.gz package like this:

```ruby

   package 'tar'
```
and a resource block that installs a tar.gz package for version 1.6.1 like this:

```ruby

   package 'tar' do
     version '1.16.1'
   end
```
In both cases, Chef Infra Client will use the default action (``:install``) to install the ``tar`` package.
