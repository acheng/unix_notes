# Azure AC-103 EXAM

## Unit 1 introduciton

## Unit 2

### What is cloud computing?

Cloud computing is the provisioning of services and applications on demand over the internet. Servers, applications, data, and other resources are provided as a service.

There are three deployment models for cloud computing: public cloud, private cloud, and hybrid cloud. 

#### Public cloud

Public clouds are the most common way of deploying cloud computing. Services are offered over the public internet and available to anyone who wants to purchase them. 

##### Why public cloud?

Public clouds can be deployed faster than on-premises infrastructures and with an almost infinitely scalable platform. 

- Service consumption through on-demand or subscription model: 
- No up-front investment of hardware: 
- Automation: 
- Geographic dispersity: 
- Reduced hardware maintenance: 

#### Private cloud

A private cloud consists of computing resources used exclusively by users from one business or organization. 

##### Why private cloud?

A private cloud can provide more flexibility to an organization. Your organization can customize its cloud environment to meet specific business needs. Since resources are not shared with others, high levels of control and security are possible. Also, private clouds can provide a level of scalability and efficiency.

Examples of why you would use private cloud:

- Pre-existing environment: An existing operating environment that can't be replicated in the public cloud. 
- Legacy applications: Business-critical legacy applications that can't easily be physically relocated.
- Data sovereignty and security: Political borders and legal requirements may dictate where data can physically exist.
- Regulatory compliance / certification: 

#### Hybrid Cloud
A hybrid cloud is a computing environment that combines a public cloud and a private cloud by allowing data and applications to be shared between them. 

##### Why hybrid cloud?

Hybrid cloud allows your organization to control and maintain a private infrastructure for sensitive assets. It also gives you the flexibility to take advantage of additional resources in the public cloud when you need them. 

Examples of why you would use hybrid cloud:

- Existing hardware investment: Business reasons require that you use an existing operating environment and hardware.
- Regulatory requirements: Regulation requires that the data needs to remain at a physical location.
- Unique operating environment: Public cloud can't replicate a legacy operating environment.
- Migration: Move workloads to the cloud over time.


## Unit 3 Cloud service models

 
Cloud computing resources are delivered using three different service models.

- Infrastructure-as-a-service (IaaS) provides instant computing infrastructure that you can provision and manage over the Internet.
- Platform as a service (PaaS) provides ready-made development and deployment environments that you can use to deliver your own cloud services.
- Software as a service (SaaS) delivers applications over the Internet as a web-based service.

When choosing a service model, consider which party should be responsible for the computing resource. 
![Could Service Model & responsibility](https://docs.microsoft.com/en-us/learn/modules/align-requirements-in-azure/media/3-shared-responsibility.png ''Cloud Service Model'')

## Unit 4 Infrastructure as a service

Infrastructure as a service (IaaS) is an instant computing infrastructure, provisioned and managed over the Internet. IaaS enables you to quickly scale resources to meet demand and only pay for what you use. IaaS avoids the expense and complexity of buying and managing your own physical servers and other datacenter infrastructure. 

ou don't need to manage physical servers and appliances. However, you are responsible for configuring and managing the components. For example, configuring firewalls, updating VM OS's, updating DBMS's, and runtimes.

#### Some other common scenarios include:

- Website hosting
- Web apps
- Storage, backup, and recovery
- High-performance computing
- Big data analysis

#### Advantages

- Eliminates capital expense and reduces ongoing cost
- Improves business continuity and disaster recovery
- Respond quicker to shifting business conditions
- Increase stability, reliability, and supportability

## Unit 5: Platform as a service

Platform as a service (PaaS) is a complete development and deployment environment in the cloud. 

### Common scenarios

Let's imagine your healthcare company needs a website to describe a product. Your developers want to use PHP. Using PaaS, your developers have the option to create a web app. The infrastructure details such as creating a virtual machine, installing a web server, and installing middleware are abstracted away. You don't need to care what operating system it runs on or what physical hardware is required. Your developers deploy the website files to the cloud and your website is available on the Internet.

Let's imagine another scenario. Your company needs a SQL database to support data analysts for a special project. You don't have infrastructure to accommodate the request. You can quickly provision a SQL Server in the cloud that meets the need of the project. The data analysts can connect to the server. The SQL Server database is provided as a service. Therefore, you don't worry about updates, security patches, or optimizing physical storage for reads and writes.

Some other common scenarios include:

- Development framework:
- Analytics or business intelligence

### Advantages

By delivering infrastructure as a service, PaaS has similar advantages as IaaS. But its additional features including middleware, development tools, and other business tools provide additional advantages:

- Reduced development time
- Develop for multiple platforms
- Use sophisticated tools affordably
- Support geographically distributed development teams
- Efficiently manage the application lifecycle

## Unit 6 Software as a service
Software as a service (SaaS) allows users to connect to and use cloud-based apps over the Internet. 

### Common scenarios

Let's imagine your healthcare company requires a customer relationship management (CRM) solution for its sales team. The team is global. You can use a SaaS CRM provider to quickly implement a solution to your organization's sales team.

For organizational use, you can rent productivity apps, such as email, collaboration, and calendaring; and sophisticated business applications such as customer relationship management (CRM), enterprise resource planning (ERP), and document management. You pay for the use of these apps by subscription or according to the level of use.

### Advantages

- Gain access to sophisticated applications
- Use free client software
- Access app data from anywhere
----

# Control Azure services with the CLI

## Unit 1 Introduciton

The Azure portal is great for performing single tasks, and to see a quick overview of the state of your resources.
But for tasks that need to be repeated daily, or even hourly, using the command line and a set of tested commands or scripts can help get your work done more quickly and avoid errors.

## Unit 2 What is Azure CLI?

The Azure CLI is a command-line program to connect to Azure and execute administrative commands on Azure resources. 

### How to install the Azure CLI

On both Linux and macOS, you use a package manager to install the Azure CLI. The recommended package manager differs by OS and distribution:

- Linux: apt-get on Ubuntu, yum on Red Hat, and zypper on OpenSUSE
- Mac: Homebrew


- On Windows, you install the Azure CLI by downloading and running an MSI file.

The Azure CLI is available in the Microsoft repository, so you'll first need to add that repository to your package manager.Using the Azure CLI in scripts

### Using the Azure CLI in scripts

If you want to use the Azure CLI commands in scripts, you need to be aware of any issues around the "shell" or environment used for running the script. For example, in a bash shell, the following syntax is used when setting variables:
```bash
variable="value"
variable=integer
```
If you use a PowerShell environment for running Azure CLI scripts, you'll need to use this syntax for variables:
```powershell
$variable="value"
$variable=integer
```
## Unit 3 Exercise - Install and run the Azure CLI

## Unit 4: Work with the Azure CLI

The Azure CLI lets you type commands and execute them immediately from the command line. 

### What Azure resources can be managed using the Azure CLI?

The Azure CLI lets you control nearly every aspect of every Azure resource.

Commands in the CLI are structured in groups and subgroups. Each group represents a service provided by Azure, and the subgroups divide commands for these services into logical groupings. For example, the `storage` group contains subgroups including `account`, `blob`, `storage`, and `queue`.

### How to create an Azure resource
- Connect
```bash
az login
```
- Create
ou'll often need to create a new resource group before you create a new Azure service, so we'll use resource groups as an example to show how to create Azure resources from the CLI.
```
az group create --name <name> --location <location>
```
- Verify
```
az group list
az group list --output table
```

#  Automate Azure tasks using scripts with PowerShell

## Unit 1: 

### What tools are available?

Azure provides three administration tools to choose from:

- The Azure portal
- The Azure CLI
- Azure PowerShell

They all offer approximately the same amount of control; any task that you can do with one of the tools, you can likely do with the other two. All three are cross-platform, running on Windows, macOS, and Linux. They differ in syntax, setup requirements, and whether they support automation.

### What is the Azure portal?

The Azure portal is a website that lets you create, configure, and alter the resources in your Azure subscription.

### What is the Azure CLI?

The Azure CLI is a cross-platform command-line program to connect to Azure and execute administrative commands on Azure resources.

### What is Azure PowerShell?

Azure PowerShell is a module that you add to Windows PowerShell or PowerShell Core to let you connect to your Azure subscription and manage resources. Azure PowerShell requires PowerShell to function. 



install powershell Azure module:
```powershell
Install-Module -Name Az -AllowClobber
```


# Predict costs and optimize spending for Azure
## Purchase Azure products and services
there are three main customer types on which the available purchasing options for Azure products and services are contingent, including:

- Enterprise - Enterprise customers sign an Enterprise Agreement with Azure that commits them to spend a negotiated amount on Azure services, which they typically pay annually. Enterprise customers also have access to customized Azure pricing.
- Web direct - Direct Web customers pay general public prices for Azure resources, and their monthly billing and payments occur through the Azure website.
- Cloud Solution Provider - Cloud Solution Provider (CSP) typically are Microsoft partner companies that a customer hires to build solutions on top of Azure. Payment and billing for Azure usage occur through the customer's CSP.

### Usage meters

When you provision an Azure resource, Azure creates one or more meter instances for that resource. The meters track the resources' usage, and generate a usage record that is used to calculate your bill.

### Factors affecting costs
- Resource Type
- Services
- Location
- Billing Zones

### Azure pricing calculator
[ Azure Price Caculator ](https://azure.microsoft.com/en-us/pricing/calculator/)

### Azure Advisor 
is a free service built into Azure that provides recommendations on high availability, security, performance, and cost.

Advisor makes cost recommendations in the following areas:

- Reduce costs by eliminating unprovisioned Azure ExpressRoute circuits.
- Buy reserved instances to save money over pay-as-you-go.
- Right-size or shutdown underutilized virtual machines. 

### Azure Cost Management

Azure Cost Management is another free, built-in Azure tool that can be used to gain greater insights into where your cloud money is going. 

### Total Cost of Ownership (TCO) calculator.
If you are starting to migrate to the cloud, a useful tool you can use to predict your cost savings is the Total Cost of Ownership (TCO) [ calculator ](https://azure.microsoft.com/pricing/tco/). 

#Control and organize Azure resources with Azure Resource Manager
## What are resource groups?
A resource group is a logical container for resources deployed on Azure. These resources are anything you create in an Azure subscription like virtual machines, Application Gateways, and CosmosDB instances. 

*All resources must be in a resource group and a resource can only be a member of a single resource group*

Many resources can be moved between resource groups with some services having specific limitations or requirements to move. Resource groups can't be nested. Before any resource can be provisioned, you need a resource group for it to be placed in.

# Use tagging to organize resources

## What are tags?

Tags are name/value pairs of text data that you can apply to resources and resource groups. Tags allow you to associate custom details about your resource, in addition to the standard Azure properties a resource has.

A resource can have up to 50 tags. The name is limited to 512 characters for all types of resources except storage accounts, which have a limit of 128 characters. The tag value is limited to 256 characters for all types of resources. Tags aren't inherited from parent resources. Not all resource types support tags, and tags can't be applied to classic resources.

Tags can be added and manipulated through the Azure portal, Azure CLI, Azure PowerShell, Resource Manager templates, and through the REST API.

# Use policies to enforce standards

## What are Azure policy?
Azure Policy is a service you can use to create, assign, and manage policies. These policies apply and enforce rules that your resources need to follow. These policies can enforce these rules when resources are created, and can be evaluated against existing resources to give visibility into compliance.

# Secure resources with role-based access control

RBAC provides fine-grained access management for Azure resources, enabling you to grant users the specific rights they need to perform their jobs.

# Use resource locks to protect resources
## What are resource locks?

Resource locks are a setting that can be applied to any resource to block modification or deletion. Resource locks can set to either Delete or Read-only. 

----------
# Secure your cloud data
# Security, responsibility, and trust in Azure 

## 4. Encryption

### What is encryption?

Encryption is the process of making data unreadable and unusable to unauthorized viewers. To use or read the encrypted data, it must be decrypted, which requires the use of a secret key. There are two top-level types of encryption: symmetric and asymmetric.

- Symmetric encryption uses the same key to encrypt and decrypt the data. Consider a desktop password manager application. You enter your passwords and they are encrypted with your own personal key (your key is often derived from your master password). When the data needs to be retrieved, the same key is used, and the data is decrypted.

- Asymmetric encryption uses a public key and private key pair. Either key can encrypt but a single key can't decrypt its own encrypted data. To decrypt, you need the paired key. Asymmetric encryption is used for things like Transport Layer Security (TLS) (used in HTTPS) and data signing.

#### Encryption at rest

Data at rest is the data that has been stored on a physical medium. 
Regardless of the storage mechanism, encryption of data at rest ensures that the stored data is unreadable without the keys and secrets needed to decrypt it.

#### Encryption in transit
Data in transit is the data actively moving from one location to another, such as across the internet or through a private network. 

### Encryption on Azure

- Encrypt raw storage

Azure Storage Service Encryption for data at rest helps you protect your data to meet your organizational security and compliance commitments. 

the handling of encryption, encryption at rest, decryption, and key management in Storage Service Encryption is transparent to applications using the services.

- Encrypt virtual machine disks

Azure Disk Encryption is a capability that helps you encrypt your Windows and Linux IaaS virtual machine disks. Azure Disk Encryption leverages the industry-standard BitLocker feature of Windows and the dm-crypt feature of Linux to provide volume encryption for the OS and data disks.
 
- Encrypt databases

Transparent data encryption (TDE) helps protect Azure SQL Database and Azure Data Warehouse against the threat of malicious activity. 

- Encrypt secrets
In Azure, we can use Azure Key Vault to protect our secrets.

Azure Key Vault is a centralized cloud service for storing your application secrets. Key Vault helps you control your applications' secrets by keeping them in a single, central location and by providing secure access, permissions control, and access logging capabilities. 

## 5. Overview of Azure certificates
Certificates used in Azure are x.509 v3 can be signed by a trusted certificate authority, or they can be self-signed. 

Certificates are used in Azure for two primary purposes and are given a specific designation based on their intended use.

   -  Service certificates are used for cloud services
   -  Management certificates are used for authenticating with the management API
   
### Using Azure Key Vault with certificates
You can store your certificates in Azure Key Vault - much like any other secret. However, Key Vault provides additional features above and beyond the typical certificate management.

   - You can create certificates in Key Vault, or import existing certificates
   - You can securely store and manage certificates without interaction with private key material.
   - You can create a policy that directs Key Vault to manage the life-cycle of a certificate.
   - You can provide contact information for notification about life-cycle events of expiration and renewal of certificate.
   - You can automatically renew certificates with selected issuers - Key Vault partner x509 certificate providers / certificate authorities.
   
## 6. Protect your network

A layered approach to network security helps reduce your risk of exposure through network-based attacks.

### What's a firewall?
To provide inbound protection at the perimeter, you have several choices.

- Azure Firewall is a managed, cloud-based, network security service that protects your Azure Virtual Network resources. It is a fully stateful firewall as a service with built-in high availability and unrestricted cloud scalability. Azure Firewall provides inbound protection for non-HTTP/S protocols. Examples of non-HTTP/S protocols include: Remote Desktop Protocol (RDP), Secure Shell (SSH), and File Transfer Protocol (FTP). It also provides outbound, network-level protection for all ports and protocols, and application-level protection for outbound HTTP/S.

- Azure Application Gateway is a load balancer that includes a Web Application Firewall (WAF) that provides protection from common, known vulnerabilities in websites. It is specifically designed to protect HTTP traffic.

- Network virtual appliances (NVAs) are ideal options for non-HTTP services or advanced configurations, and are similar to hardware firewall appliances.

### Stopping Distributed Denial of Service (DDos) attacks

hese types of attacks attempt to overwhelm a network resource by sending so many requests that the resource becomes slow or unresponsive.

When you combine Azure DDoS Protection with application design best practices, you help provide defense against DDoS attacks. 

Azure DDoS Protection provides the following service tiers:

   - Basic. The Basic service tier is automatically enabled as part of the Azure platform. Always-on traffic monitoring and real-time mitigation of common network-level attacks provide the same defenses that Microsoft's online services use. Azure's global network is used to distribute and mitigate attack traffic across regions.
   - Standard. The Standard service tier provides additional mitigation capabilities that are tuned specifically to Microsoft Azure Virtual Network resources. DDoS Protection Standard is simple to enable and requires no application changes. Protection policies are tuned through dedicated traffic monitoring and machine learning algorithms. Policies are applied to public IP addresses which are associated with resources deployed in virtual networks, such as Azure Load Balancer and Application Gateway. DDoS standard protection can mitigate the following types of attacks:
        Volumetric attacks. The attackers goal is to flood the network layer with a substantial amount of seemingly legitimate traffic.
        Protocol attacks. These attacks render a target inaccessible, by exploiting a weakness in the layer 3 and layer 4 protocol stack.
        Resource (application) layer attacks. These attacks target web application packets to disrupt the transmission of data between hosts.
		
### Controlling the traffic inside your virtual network

Network Security Groups allow you to filter network traffic to and from Azure resources in an Azure virtual network. An NSG can contain multiple inbound and outbound security rules that enable you to filter traffic to and from resources by source and destination IP address, port, and protocol.

### Network integration
Virtual private network (VPN) connections are a common way of establishing secure communication channels between networks. Connection between Azure Virtual Network and an on-premises VPN device is a great way to provide secure communication between your network and your VNet on Azure.


## 7. Protect your shared documents

Microsoft Azure Information Protection (sometimes referred to as AIP) is a cloud-based solution that helps organizations classify and optionally protect documents and emails by applying labels.

Labels can be applied automatically based on rules and conditions, manually, or a combination of both where users are guided by recommendations.

## 8. Azure Advanced Threat Protection
Azure Advanced Threat Protection (Azure ATP) is a cloud-based security solution that identifies, detects, and helps you investigate advanced threats, compromised identities, and malicious insider actions directed at your organization.

Azure ATP is capable of detecting known malicious attacks and techniques, security issues, and risks against your network.
Azure ATP components

Azure ATP consists of several components.

- Azure ATP portal
Azure ATP has its own portal, through which you can monitor and respond to suspicious activity.

- Azure ATP sensor

Azure ATP sensors are installed directly on your domain controllers. The sensor monitors domain controller traffic without requiring a dedicated server or configuring port mirroring.

- Azure ATP cloud service

Azure ATP cloud service runs on Azure infrastructure and is currently deployed in the United States, Europe, and Asia. Azure ATP cloud service is connected to Microsoft's intelligent security graph.

## 9. Understand Security Considerations for Application Lifecycle Management Solutions

### Provide training

Security is everyone's job.

### Define security requirements

Considering security and privacy is a fundamental aspect of developing highly secure applications and systems.
Factors that influence security requirements include, but are not limited to:

 -   Legal and industry requirements
 -   Internal standards and coding practices
 -   Review of previous incidents
 -   Known threats
 
### Define metrics and compliance reporting

It's essential for an organization to define the minimum acceptable levels of security quality, and to hold engineering teams accountable to meeting that criteria. Defining these expectations early helps a team understand the risks that are associated with security issues, identify and fix security defects during development, and apply the standards throughout the entire project. 

### Perform threat modeling

Threat modeling should be used in environments where there is a meaningful security risk.

### Establish design requirements
The SDL is typically thought of as assurance activities that help engineers implement more secure features, meaning the features are well engineered with respect to security. 

### Define and use cryptography standards

### Manage security risks from using third-party components

### Use approved tools

### Perform Static Analysis Security Testing

### Perform Dynamic Analysis Security Testing

### Perform penetration testing

### Establish a standard incident response process

# 	Top 5 security items to consider before pushing to production

## What is Azure Security Center?

Azure Security Center (ASC) is a monitoring service that provides threat protection across all of your services both in Azure, and on-premises.

It can:

  -  Provide security recommendations based on your configurations, resources, and networks.
  -  Monitor security settings across on-premises and cloud workloads and automatically apply required security to new services as they come online.
  -  Continuously monitor all your services and perform automatic security assessments to identify potential vulnerabilities before they can be exploited.
  -  Use machine learning to detect and block malware from being installed in your services and virtual machines. You can also white-list applications to ensure that only the apps you validate are allowed to execute.
  -  Analyze and identify potential inbound attacks and help to investigate threats and any post-breach activity which might have occurred.
  -  Just-In-Time access control for ports, reducing your attack surface by ensuring the network only allows traffic you require.
  
## Activating Azure Security Center

offered in two tiers: Free and Standard. 
The free tier provides security policies, assessments, and recommendations while the Standard tier provides a robust set of features, including threat intelligence.

### Free vs. Standard pricing tier

While you can use a free Azure subscription tier with ASC, it is limited to assessments and recommendations of Azure resources only. 




## Inputs and Outputs

The most prevalent security weakness of current applications is a failure to correctly process data that is received from external sources, particularly user input.

### Why do we need to validate our input?
It could contain mailicious code

### When do I need to validate input?

The answer is always. 

### Always use parameterized queries

stored procedures in SQL
Parameterized queries substitute variables before running queries, meaning it avoids the opportunity for code to be submitted in place of a variable.

## Secrets in Key Vault

Azure Key Vault is a secret store: a centralized cloud service for storing application secrets. Key Vault keeps your confidential data safe by keeping application secrets in a single central location and providing secure access, permissions control, and access logging.

Secrets are stored in individual vaults, each with their own configuration and security policies to control access. You can then get to your data through a REST API, or through a client SDK available for most languages.

Key Vault allows users to store connection strings, secrets, passwords, certificates, access policies, file locks (making items in Azure read-only), and automation scripts. It also logs access and activity, allows you to monitor access control (IAM) in your subscription, and it also has diagnostic, metrics, alerts and troubleshooting tools, to ensure you have the access you need.

## Framework Updates

### Choose your framework carefully
The most important factor regarding security when choosing a framework is how well supported it is. 

### Keep your framework updated

### Take advantage of built-in security

Always check to see what security features your frameworks offer. Never roll your own security if there's a standard technique or capability built in. In addition, rely on proven algorithms and workflows because these have often been scrutinized by many experts, critiqued and strengthened so you can be assured that they are reliable and secure.

### Azure Security Center

When using Azure to host your web applications, Security Center will warn you if your frameworks are out of date as part of the recommendations tab. Don't forget to look there from time to time to see if there are any warnings related to your apps.

## Safe Dependencies

### Track known security vulnerabilities

The problem we have is knowing when an issue is discovered. Keeping our libraries and dependencies updated (#4 in our list!) will of course help, but it's a good idea to keep track of identified vulnerabilities that might impact your application.

### How to verify if you have known vulnerabilities in your 3rd party components
### Always encode your output

# configure security policies to manage data

Classifying your data and identifying your data protection needs helps you select the right cloud solution for your organization.

## Classify your data at rest, in process, and in transit

Digital data always exists in one of three states: at rest, in process, and in transit.

All three states require unique technical solutions for data classification, but the applied principles of data classification should be the same for each. Data that is classified as confidential needs to stay confidential in each state.

Data can also be either structured or unstructured. Typical classification processes for structured data found in databases and spreadsheets are less complex and time-consuming to manage than those for unstructured data such as documents, source code, and email. 

### Protect data at rest

Data encryption at rest is a mandatory step toward data privacy, compliance, and data sovereignty.

### Protect data in transit
we generally recommend that you always use SSL/TLS protocols to exchange data across different locations. In some circumstances, you might want to isolate the entire communication channel between your on-premises and cloud infrastructures by using a VPN.

### Data discovery
Data discovery and classification (currently in preview) provides advanced capabilities built into Azure SQL Database for discovering, classifying, labeling and protecting sensitive data (such as business, personal data (PII), and financial information) in your databases.

Data discovery and classification is part of the Advanced Data Security offering, which is a unified package for advanced Microsoft SQL Server security capabilities. You access and manage data discovery and classification via the central SQL Advanced Data Security portal.

## Explore data recovery, retention, and disposal
Once an organization's data has been examined and classified, the next decision to make is how long to keep the data around.
A data retention policy should address the required regulatory and compliance requirements and corporate legal retention requirements
### Immutable storage and data retention

Immutable storage for Azure Blob Storage enables users to store business-critical data in a write once, read many (WORM) state. This state makes the data unerasable and unmodifiable for a user-specified interval. Blobs can be created and read, but not modified or deleted, for the duration of the retention interval.

## Understand data sovereignty
Digital information is always subject to the laws of the country or region where it's stored. This concept is known as data sovereignty. Many of the concerns that surround data sovereignty relate to enforcing privacy regulations and preventing data that are stored in a foreign country from being subpoenaed by the host country or region's government.

Paired regions

Azure operates in multiple geographies around the world. Azure geography is a defined area of the world that contains at least one Azure Region. An Azure region is an area containing one or more data centers.

Cross-region activities number key

    Azure Compute (IaaS) - You must provision additional compute resources in advance to ensure resources are available in another region during a disaster. For more information, see Designing resilient applications for Azure.
    Azure Storage- Geo-redundant storage (GRS) is configured by default when an Azure Storage account is created. With GRS, data is automatically replicated three times within the primary region, and three times in a paired region. For more information, see Azure Storage redundancy.
    Azure SQL Database - With Azure SQL Database geo-replication, you can configure asynchronous replication of transactions to any region in the world; however, we recommend you deploy these resources in a paired region for most disaster recovery scenarios. For more information, see Configure active geo-replication for Azure SQL Database in the Azure portal.
    Azure Resource Manager - Resource Manager inherently provides logical isolation of components across regions. This means that logical failures in one region are less likely to impact other regions.

Benefits of Azure paired regions number key:

    Physical isolation - When possible, Azure services prefer at least 300 miles of separation between datacenters in a regional pair (although this isn't practical or possible in all geographies). Physical datacenter separation reduces the likelihood of both regions being affected simultaneously as a result of natural disasters, civil unrest, power outages, or physical network outages. Isolation is subject to the constraints within the geography, such as geography size, power, and network infrastructure availability, and regulations.
    Platform-provided replication - Some services such as geo-redundant storage provide automatic replication to the paired region.
    Region recovery order - In the event of a widespread outage, recovery of one region is prioritized out of every pair. Applications that are deployed across paired regions are guaranteed to have one of the regions recovered with priority. If an application is deployed across regions that are not paired, recovery might be delayed. In the worst case, the chosen regions might be the last two to be recovered.
    Sequential updates - Planned Azure system updates are rolled out to paired regions sequentially, not at the same time. This helps minimize downtime, the effect of bugs, and logical failures in the rare event of a bad update.
    Data residency - To meet data residency requirements for tax and law enforcement jurisdiction purposes, a region resides within the same geography as its pair (with the exception of Brazil South).


# Secure your Azure Storage account

Azure Storage accounts provide a wealth of security options that protect your cloud-based data. Azure services such as Blob storage, Files share, Table storage, and Data Lake Store all build on Azure Storage. Because of this foundation, the services benefit from the fine-grained security controls in Azure Storage.

Learning objectives

In this module you will:

    Investigate the ways Azure Storage protects your data
    Explore the authentication options to access data
    Learn about Advanced Threat Protection
    Learn how to control network access to data
    Explore the Azure Data Lake enterprise-class security features

## Explore Azure Storage security features

### Encryption at rest
All data written to Azure Storage is automatically encrypted by Storage Service Encryption (SSE) with a 256-bit Advanced Encryption Standard (AES) cipher. SSE automatically encrypts data when writing it to Azure Storage. When you read data from Azure Storage, Azure Storage decrypts the data before returning it. This process incurs no additional charges and doesn't degrade performance. It can't be disabled.

For virtual machines (VMs), Azure lets you encrypt virtual hard disks (VHDs) by using Azure Disk Encryption. This encryption uses BitLocker for Windows images, and it uses dm-crypt for Linux.

Azure Key Vault stores the keys automatically to help you control and manage the disk-encryption keys and secrets. So even if someone gets access to the VHD image and downloads it, they can't access the data on the VHD.

### Encryption in transit
Keep your data secure by enabling transport-level security between Azure and the client.

Azure Storage supports cross-domain access through cross-origin resource sharing (CORS). CORS uses HTTP headers so that a web application at one domain can access resources from a server at a different domain. By using CORS, web apps ensure that they load only authorized content from authorized sources.

CORS support is an optional flag you can enable on Storage accounts. The flag adds the appropriate headers when you use HTTP GET requests to retrieve resources from the Storage account.

### Role-based access control

Azure Storage supports Azure Active Directory and role-based access control (RBAC) for both resource management and data operations. To security principals, you can assign RBAC roles that are scoped to the storage account. Use Active Directory to authorize resource management operations, such as configuration. Active Directory is supported for data operations on Blob and Queue storage.

To a security principal or a managed identity for Azure resources, you can assign RBAC roles that are scoped to a subscription, a resource group, a storage account, or an individual container or queue.

### Auditing access

Auditing is another part of controlling access. You can audit Azure Storage access by using the built-in Storage Analytics service.

Storage Analytics logs every operation in real time, and you can search the Storage Analytics logs for specific requests. Filter based on the authentication mechanism, the success of the operation, or the resource that was accessed.

## Understand storage account keys

In Azure Storage accounts, shared keys are called storage account keys. Azure creates two of these keys (primary and secondary) for each storage account you create. The keys give access to everything in the account.

The storage account has only two keys, and they provide full access to the account. Because these keys are powerful, use them only with trusted in-house applications that you control completely.

## Understand shared access signatures

As a best practice, you shouldn't share storage account keys with external third-party applications. If these apps need access to your data, you'll need to secure their connections without using storage account keys.

For untrusted clients, use a shared access signature (SAS). A shared access signature is a string that contains a security token that can be attached to a URI. Use a shared access signature to delegate access to storage objects and specify constraints, such as the permissions and the time range of access.

### Types of shared access signatures

- service-level
You can use a service-level shared access signature to allow access to specific resources in a storage account.
- account-level
Use an account-level shared access signature to allow access to anything that a service-level shared access signature can allow, plus additional resources and abilities. For example, you can use an account-level shared access signature to allow the ability to create file systems.

## Control network access to your storage account

By default, storage accounts accept connections from clients on any network. To limit access to selected networks, you must first change the default action. You can restrict access to specific IP addresses, ranges, or virtual networks.

## Understand Advanced Threat Protection for Azure Storage

Detecting threats to your data is an important part of security. You can check an audit trail for all activity against a storage account. But that will often only show you that an intrusion has already occurred. What you really want is a way to be notified when suspicious activity is happening. That's where the Advanced Threat Protection feature in Azure Storage can help.

Advanced Threat Protection, now in public preview, detects anomalies in account activity. It then notifies you of potentially harmful attempts to access your account. You don't have to be a security expert or manage security monitoring systems to take advantage of this layer of threat protection.

Currently, Advanced Threat Protection for Azure Storage is available for the Blob service. Security alerts are integrated with Azure Security Center. The alerts are sent by email to subscription admins.