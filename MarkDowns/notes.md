#### molecule test options how wether or not to destroy docker instance

```bash
molecule test --destroy [always,never,passing]
```

#### query AD using ldapsearch tool
```bash
ldapsearch -x -h 10.22.17.50 -D "maximo@hkld.local" -w 'P@ssw0rd'  -b "dc=hkld,dc=local" -s sub "(&(objectCategory=person)(objectClass=user)(sAMAccountName=sophie))" objectGUID objectSID
# 10.22.17.50 is DC


#### reading system log with journalctl
	journalctl --no-pager  # to fix line wrapp issue
	journalctl -u docker.service  ## to see only docker related log
	journalctl -r ## reverse display. lastest log first
	journalctl --since 2017-07-17
	journalctl -f  ## same as tail -f, follow logs
	journalctl -e ## jump to the end of pager
	journalctl -x  ## display some help msg when available
```
#### troubleshoot joining mac to AD
```bash
sudo odutil set log debug
sudo odutil set log default
kinit $user@domain.name
dsconfigad
```

#### enable SSHD on mac, from command line
sudo systemsetup -setremotelogin on
	
#### attach to a docker instance runing in background
```bash
docker exec -i -t $ID /bin/bash

#### save docker image
docker save -o myimg.tar

#### export docker image
docker export -o myimg.tar

#### check docker restart policy

docker inspect -f "{{ .HostConfig.RestartPolicy }}" ${container_name}
# RestartPolicy has two properties Name, MaximumRetryCount

#### make docker start with system
docker update --restart always admiring_mirzakhani

#### import docker image with tag
docker import myimg.tar -c "ENTRYPOINT docker-entrypoint.sh" myimg:v0.5.6

#### run phpmyadmin docker on mysql host

docker run --name phpmyadmin -d -e PMA_HOST=10.22.16.18 -e PMA_USER=cms -e PMA_PASSWORD=hkldc -p 8080:80 phpmyadmin/phpmyadmin


#### check docker container logs
docker logs $container_name
```
#### how to exclude files when creating archive
```bash
tar -czf archive_file.tar.gz target --exclude="*.zip" --exclude="*.war"

#### mount windows shares on CentOS 7
mount.cifs --verbose -o username='alan_cheng',\
password='yourPass',sec=ntlm \
\\\\epbyminw1383.minsk.epam.com\\install_VEL /mnt

#### mount windows shares on Ubuntu 17 (no pwd required)
mount -t cifs //server/share /mnt

#### list samba users in its own database
pdbedit -L
```
#### reset author info after you made a commit
# first, use git config [--global] user.name and user.email to set
# your name and email, then:

```bash
git commit --amend --reset-author

#### show all git commits in master that aren’t in experiment 

git log experiment..master

#### git checkout a single file (core.py) from another branch (e.g dev)

git checkout dev -- core.py

#### change last commit msg in git

git commit --amend
git push --force ## if previous commit msg has been pushed out

#### disable SSL cert verify globally
git config --global http.sslVerify "false"
	
```
#### start configure sound device GUI
```bash
pavucontrol
#### add static route via IP command
sudo ip route add 192.168.0.0/24 via 10.22.17.55 dev eno1


#### check deb package dependencies

dpkg-dep -I pckage.deb


#### ubuntu add i386 arch on 64bit system

dpkg --add-architecture i386


#### reconfigure lxd network (lxd init has been executed before)

sudo dpkg-reconfigure -p medium lxd

#### configure lxd to use physical network (bridge)
lxc profile device set default eth0 parent eno1
lxc profile device set default eth0 nictype macvlan


#### configure lxd container to start at system boot
lxc config set machine boot.autostart true
```

#### manage virtualbox on CLI
```bash
vboxmanage list vms
vboxmanage import vmname.ova
vboxmanage startvm vmname --type headless
VBoxManage extpack install name.extpack
vboxmanage guestcontrol "<vbox_name>" updateadditions  --source /usr/share/virtualbox/VBoxGuestAdditions.iso --verbose
vboxmanage guestproperty get "vm-name" "/VirtualBox/GuestInfo/Net/0/V4/IP"
vboxmanage showvminfo WIN10_64
vboxmanage controlvm "OpenBSD6.1" acpipowerbutton 
vboxmanage modifyvm "OpenBSD6.1" --nic1 bridged
vboxmanage modifyvm "OpenBSD6.1" --bridgeadapter1 eno1
```

#### reset mysql root password
```mysql
#For MySQL 5.7.5 and older as well as MariaDB 10.1.20 and older, use:
SET PASSWORD FOR 'root'@'localhost' = PASSWORD('new_password');

#For MySQL 5.7.6 and newer as well as MariaDB 10.1.20 and newer, use the following command.
ALTER USER 'root'@'localhost' IDENTIFIED BY 'new_password';

#or a more traditional method
UPDATE mysql.user SET authentication_string = PASSWORD('new_password') WHERE User = 'root' AND Host = 'localhost';
```


#### run one time scheduled task with at
```bash
at -f ~/myscript.sh 8PM

# or input command from stdin
at 10PM
at> echo "this is just an example"
at> ls -l ~/Documents
^D

# use atq to view task queues
atq

# use atrm to delete task by ID
atrm 4
```
#### lsof frequently used arguments
```bash
#Which processes have an open socket on a TCP port
lsof -i :8080

# Which processes have an open TCP socket to www.google.com
lsof -iTCP@www.google.com:80

# Show connec­tions to a host
lsof -i@192.16­8.1.5

# Find ports that are being listened to
lsof -i| grep LISTEN

# Which processes are accessing the Mysql socket?
lsof /var/run/mysql/mysql.sock

# Which processes have the nginx binary open
lsof /usr/sbin/nginx

# find files open by user
lsof -u $USER

# find files open by proccess id
lsof -p $PID

# get list of processes in use by command
lsof -t -c <co­mma­nd>

# Open files within a directory
lsof +D /path

# By program name
lsof -c apache

# AND'ing selection conditions
lsof -u www-data -c apache

# NFS use
lsof -N 

# UNIX domain socket use
lsof -U 
```

#### bash variable substitution rules
```bash

# '${var#Pattern}' Remove from $var the 'shortest' part of $Pattern that matches the front end of $var.

# '${var##Pattern}' Remove from $var the 'longest' part of $Pattern that matches the front end of $var.
---

# '${var%Pattern}' Remove from $var the shortest part of $Pattern that matches the 'back' end of $var.


# '${var%%Pattern}' Remove from $var the longest part of $Pattern that matches the 'back' end of $var.


#  '*' is a wildcard

var=abcd-1234-defg
echo ${var#*-*}  # result: 1234-defg
echo ${var#*-}   # result: 1234-defg  '*' can mean nothing

# ${var:pos}
# Variable var expanded, starting from offset pos.

# ${var:pos:len}
# Expansion to a max of len characters of variable var, from offset pos. 
path_name="/home/bozo/ideas/thoughts.for.today"
t=${path_name:11}  ## ideas/thoughts.for.today
echo "$path_name, with first 11 chars stripped off = $t"

t=${path_name:11:5}  ## ideas
echo "$path_name, with first 11 chars stripped off, length 5 = $t"
# ${var/Pattern/Replacement}
# First match of Pattern, within var replaced with Replacement.

# If Replacement is omitted, then the first match of Pattern is replaced by nothing, that is, deleted.

# ${var//Pattern/Replacement}
# Global replacement. All matches of Pattern, within var replaced with Replacement.

# As above, if Replacement is omitted, then all occurrences of Pattern are replaced by nothing, that is, deleted.

# ${var/#Pattern/Replacement}
# If prefix of var matches Pattern, then substitute Replacement for Pattern.

# ${var/%Pattern/Replacement}
# If suffix of var matches Pattern, then substitute Replacement for Pattern.

# ${!varprefix*}, ${!varprefix@}
# “Matches names of all previously declared variables beginning with varprefix.”

xyz23=whatever
xyz24=

a=${!xyz*}         #  Expands to *names* of declared variables
# ^ ^   ^           + beginning with "xyz".
echo "a = $a"      #  a = xyz23 xyz24
a=${!xyz@}         #  Same as above.
echo "a = $a"      #  a = xyz23 xyz24

echo "---"

abc23=something_else
b=${!abc*}
echo "b = $b"      #  b = abc23
c=${!b}            #  Now, the more familiar type of indirect reference.
echo $c            #  something_else
```


#### FFMPEG stuff

```bash


#### disable subtitles by default, :s1 means first subtitle track ...
mkvpropedit  --edit track:s1 --set flag-default=0 --edit track:s2 --set flag-default=0 video.mkv

#### set 2nd (and last!) sound track to default
mkvpropedit  --edit track:a1 --set flag-default=0 --edit track:a2 --set flag-default=1 video.mkv

#### video scaling with ffmpeg
ffmpeg -i input.mp4 -vf scale=1280:720 output.mp4

#### To increase the volume of the first audio track for 10dB use:
ffmpeg -i inputfile -vcodec copy -af "volume=10dB" outputfile

#### crop video
ffmpeg -i in.mp4 -filter:v "crop=out_w:out_h:x:y" out.mp4
 # out_w is the width of the output rectangle
 # out_h is the height of the output rectangle
 # x and y specify the top left corner of the output rectangle


#### convert VOB to mkv format with ffmpeg
ffmpeg \
  -analyzeduration 1000M -probesize 1000M \
  -i 04.vob \
  -max_muxing_queue_size 9999 \
  -map 0:1 -map 0:2 -map 0:3 -map 0:4 \
  -metadata:s:a:0 language=cantonese -metadata:s:a:0 title="Cantonese stereo" \
  -metadata:s:s:1 language=Cantonese -metadata:s:s:1 title="Cantonese" \
  -metadata:s:s:0 language=Mandrin -metadata:s:s:0 title="Mandrin" \
  -codec:v libx264 -crf 21 \
  -codec:a libmp3lame -qscale:a 2 \
  -codec:s copy \
  04.mkv
```

#### copy and paste action in tmux EMACS mode

^b + [ to enter copy mode
^Space to start selection
Alt + w to mark end of selection
^y to paste buffer
^b + # to list buffer
^b + = to select which buffer to paste

#### tmux copy and paste in VI mode
^b + [ to enter copy mode
Space to start selection
Enter to end selection and copy
^b + ] to paste

#### scroll copy & paste in screen 

Maybe there's a better way, but I'm used to scrolling using the "copy mode" (which you can use to copy text using screen itself, although that requires the paste command too):

Hit your screen prefix combination (C-a / control+A by default), then hit `Escape`.

Move up/down with the `arrow keys` (↑ and ↓).

When you're done, hit q or Escape to get back to the end of the scroll buffer.

(If instead of q or Escape you hit Enter or Return and then move the cursor, you will be selecting text to copy, and hitting Enter or Return a second time will copy it. Then you can paste with C-a followed by ].)

Of course, you can always use more and less, two commonly used pagers, which may be enough for some commands.

#### use keys with rsync
```bash
rsync -av -e 'ssh -i sshkey.pem' config centos@qadb:/tmp/

#### update grub on centos 7
grub2-mkconfig -o "$(readlink /etc/grub2-efi.cfg)"


#### list rpm package dependencies
yum deplist package

#### remove package without its dependencies
rpm -e --nodeps $i
```

#### iscsi commands
```bash
iscsiadm -m node -logout
iscsiadm -m session -o show   (check no sessions)
#Kill session    
iscsiadm -m node -T <iqn> -p <ip address>:<port number> -u (optional)
iscsiadm -m node -o delete -T iqn.2007-06.com.test.geeklab:storage.geeklab --portal 192.168.10.12:3260    (delete target, optional)
# Delete discovery db, optional 
iscsiadm -m discoverydb -t sendtargets -p <IP>:<port> -o delete
# stop service
systemctl iscsid stop ; systemctl iscsi stop

# modify configure /etc/iscsi/iscsid.conf
iscsiadm -m discovery -t sendtargets -p  IP1/IP2/IP3/IP4

## login
scsiadm -m node –login
## login to a specified target
iscsiadm -m node --targetname iqn.2019-11.com.vmware.iscsi:sin-sc-perf04-2 -p  10.205.246.42 --login
#Logging in to [iface: default, target: iqn.2019-11.com.vmware.iscsi:sin-sc-perf04-2, portal: 10.205.246.42,3260] (multiple)
#Login to [iface: default, target: iqn.2019-11.com.vmware.iscsi:sin-sc-perf04-2, portal: 10.205.246.42,3260] successful.

## rescan
iscsiadm -m session --rescan
```
## misc stuff
```bash
for i in $(jq '.|keys' _hosts-ncsa.json  | grep 'sc-perf' | sed 's/"//g;s/,//g'); do echo -n "$i ...";ssh -q -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null $i "chef-client --version"; done
```

ip addr del 10.0.14.123/23 dev em1