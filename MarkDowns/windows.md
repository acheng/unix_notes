## Some windows related stuff
----

#### export DNS zone file using PowerShell
	PS C:\> Export-DnsServerZone -Name "western.contoso.com" -FileName "exportedcontoso.com"
	# file will be saved in C:\windows\system32\DNS
	
#### configure system env for JAVA_HOME in command line, on Windows

	setx /m JAVA_HOME "C:\Program Files\Java\jdk1.8.0_131"

#### display dynamic port ranges

	netsh int ipv4 show dynamicport tcp

#### set dynamic port ranges	
	netsh int ipv4 set dynamicportrange protocol=tcp startport=10000 numberofports=20000
	
#### query AD user by First Name(GivenName) and Last name (Surname):
	Add-WindowsCapability -online -Name "Rsat.ActiveDirectory.DS-LDS.Tools" #add module on Windows 10
	Get-ADUser -Filter "GivenName -eq 'Guillaume' -and Surname -eq 'Broche'"