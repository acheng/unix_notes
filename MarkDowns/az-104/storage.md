## Azure Storage Account:
	include 4 services: 
	- Files
	- Blobs
	- Queues
	- Tables
	
### Blobs
Blobs are "files for the cloud". Apps work with blobs in much the same way as they would work with files on a disk, like reading and writing data. However, unlike a local file, blobs can be reached from anywhere with an internet connection.

Azure Blob storage is *unstructured*, meaning that there are no restrictions on the kinds of data it can hold. 
For example, a blob can hold a PDF document, a JPG image, a JSON file, video content, etc.

Blobs aren't limited to common file formats — a blob could contain gigabytes of binary data streamed from a scientific instrument, an encrypted message for another application, or data in a custom format for an app you're developing.

Blobs are usually *not appropriate* for structured data that needs to be queried frequently.

They have *higher latency* than memory and local disk and *don't have the indexing features* that make databases efficient at running queries.


#### Storage accounts, containers, and metadata
In Blob storage, *every blob lives inside a blob container*. You can store an *unlimited number of blobs in a container* and an unlimited number of containers in a storage account. 
Containers are "flat" — they can only store blobs, not other containers.

Blobs and containers support metadata in the form of name-value string pairs.

The Blob storage API is REST-based and supported by client libraries in many popular languages.

##### Naming limitations
Container and blob names must conform to a set of rules, including length limitations and character restrictions.

#### Blob types
There are three different kinds of blobs you can store data in:
- *Block blobs* are composed of blocks of different sizes that can be uploaded independently and in parallel. Writing to a block blob involves uploading data to blocks and committing them to the blob.
- *Append blobs* are specialized block blobs that support only appending new data (not updating or deleting existing data), but they're very efficient at it. Append blobs are great for scenarios like storing logs or writing streamed data.
- *Page blobs* are designed for scenarios that involve random-access reads and writes. Page blobs are used to store the virtual hard disk (VHD) files used by Azure Virtual Machines, but they're great for any scenario that involves random access.

### Azure Files 
provides a cloud-based file share for storing and sharing files to applications. 
You then access these files from applications hosted in Azure App Service, an Azure VM, or an on-premises machine. Azure Files stores and shares file access between applications and systems in a secure and failure-resilient manner.


## Structured data
Structured data, sometimes referred to as relational data, is data that adheres to a strict schema, so all of the data has the same fields or properties. The shared schema allows this type of data to be easily searched with query languages such as SQL (Structured Query Language). This capability makes this data style perfect for applications such as CRM systems, reservations, and inventory management.

## Semi-structured data
Semi-structured data is less organized than structured data, and is not stored in a relational format, as the fields do not neatly fit into tables, rows, and columns. Semi-structured data contains tags that make the organization and hierarchy of the data apparent - for example, key/value pairs. Semi-structured data is also referred to as non-relational or NoSQL data. The expression and structure of the data in this style is defined by a serialization language.

### Common formats
- XML
- JSON
- YAML

### Unstructured data
The organization of unstructured data is ambiguous. Unstructured data is often delivered in files, such as photos or videos. The video file itself may have an overall structure and come with semi-structured metadata, but the data that comprises the video itself is unstructured. Therefore, photos, videos, and other similar files are classified as unstructured data.
Examples of unstructured data include:
- Media files, such as photos, videos, and audio files
- Office files, such as Word documents
- Text files
- Log files

### Data redundancy
Data in Azure is replicated to ensure that it's always available, even if a datacenter or region becomes inaccessible or a specific piece of hardware fails. You have four replication options:
- Locally redundant storage (LRS)
- Zone-redundant storage (ZRS)
- Geographically redundant storage (GRS)
- Read-access geo-redundant storage (RA-GRS)

Locally redundant storage replicates data and stores three copies across fault domains, or racks of hardware, within a single datacenter facility in one region. Data is replicated so that if there's a hardware fault or maintenance work, your data is still available and accessible.

Zone-redundant storage replicates your data across three storage clusters in a region. Each cluster is physically separated from the other two, which means that each cluster is supplied by separate utilities, such as power or networking.

Geographically redundant, or geo-redundant, storage provides multiple levels of replication. Your data is replicated three times within the primary region, and then this set is replicated to a secondary region.

Keep in mind that your data in the secondary region is inaccessible until the primary region has failed across to the secondary region.

Read-access geo-redundant storage (RA-GRS) is the same as GRS, but offers read access in the secondary datacenter in the other region

#### Implications of a storage account failover
When a storage account failover occurs, you could lose data. Data from the current primary region might not replicate across to the secondary region at the time of the failover. To determine whether there's likely to be data loss, you can check the Last Sync Time property. You used the command for finding this value in the previous exercise to review the storage account replication status.

*Failover is automatic and controlled by Microsoft. A manual failover of an Azure storage account isn't possible in a majority of the Azure regions. However, Microsoft has made a new feature available in WestUS2 and CentralUS regions, with which you can manually failover the storage account*

