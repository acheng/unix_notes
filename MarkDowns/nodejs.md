#### node install
```
curl -sL https://deb.nodesource.com/setup_7.x | sudo -E bash -
sudo apt-get install -y nodejs
```

#### yarn install
```
curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -

echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list

sudo apt-get update && sudo apt-get install yarn
```

#### NPM commands
```bash
#set registry
sudo npm config set registry https://artifactory.ubisoft.org/api/npm/npm/
npm config ls -l ## list config
npm list -g ## list globally installed packages

```

#### add new user to svn via httpd basic auth
```shell
htpasswd -m /etc/httpd/subversion-auth cs2
```