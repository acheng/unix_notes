## what is stackstorm?
StackStorm is a platform for integration and automation across services and tools. It ties together your existing infrastructure and application environment so you can more easily automate that environment. It has a particular focus on taking actions in response to events.

or, an automatic response system to various events 

## terms & jargons

### Actions
Actions are pieces of code that can perform arbitrary automation or remediation tasks in your environment. They can be written in any programming language.

To give you a better idea, here is a short list of tasks which can be implemented as actions:
restart a service on a server
create a new cloud server
acknowledge a Nagios/PagerDuty alert
send a notification or alert via email or SMS
send a notification to an IRC channel
send a message to Slack
start a Docker container
snapshot a VM
run a Nagios check

Actions can be executed when a *Rule* with matching criteria is triggered. Multiple actions can be strung together into a Workflow. Actions can also be executed directly from the clients via CLI, API, or UI.

### Sensors
Sensors are a way to integrate external systems and events with StackStorm. Sensors are pieces of Python code that either periodically poll some external system, or passively wait for inbound events. They then inject triggers into StackStorm, which can be matched by rules, for potential action execution.

### Triggers
Triggers are StackStorm constructs that identify the incoming events to StackStorm. A trigger is a tuple of type (string) and optional parameters (object). Rules are written to work with triggers. Sensors typically register triggers though this is not strictly required. For example, there is a generic webhooks trigger registered with StackStorm, which does not require a custom sensor.

### Internal Triggers
By default StackStorm emits some internal triggers which you can leverage in rules. Those triggers can be distinguished from non-system triggers since they are prefixed with `st2.`.


### Rules
StackStorm uses rules and workflows to capture operational patterns as automations. Rules map triggers to actions (or workflows), apply matching criteria and map trigger payloads to action inputs.

### Workflows
A workflow strings atomic actions into a higher level automation, and orchestrates their executions by calling the right action, at the right time, with the right input. It keeps state, passes data between actions, and provides reliability and transparency to the execution.

### Packs
A “pack” is the unit of deployment for integrations and automations that extend StackStorm.
A pack can contain Actions, Workflows, Rules, Sensors, and Aliases.
Some packs extend StackStorm to integrate it with external systems — like AWS, GitHub, or JIRA. We call them “integration packs”. Some packs capture automation patterns — they contain workflows, rules, and actions for a specific automation process — like the st2 demo pack. We call them “automation packs”. 

### Webhooks
Webhooks allow you to integrate external systems with StackStorm using HTTP webhooks.

Sensors vs Webhooks
Sensors integrate with external systems and services using either a polling approach (sensors periodically connect to an external system to retrieve data), or a passive approach, where they listen on some port, receiving data using whatever custom protocol you define.

Webhooks provide a built-in passive approach for receiving JSON or URL-encoded form data, via HTTP POST. This data must be “pushed” from an external system to StackStorm when an interesting event occurs.

### Datastore
The goal of the datastore service is to allow users to store common parameters and their values within StackStorm for reuse in the definition of sensors, actions, and rules. The datastore service stores the data as a key-value pair. They can be get/set using the StackStorm CLI or the StackStorm Python client.

### ChatOps
ChatOps provides a new way of collaboration and work within your teams. Boiled down to its essence, ChatOps represents conversation-driven development. With ChatOps, it is possible to take operational and development workflows and expose them as commands that can be executed in a company chat room. In doing so, you are unifying the communication about what work should get done with the actual history of the work being done.

Deploying code from Chat, viewing graphs from a TSDB or logging tool, or creating new Jira tickets are all examples of tasks that can be done via ChatOps.